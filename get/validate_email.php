<?php
if (!isset($_GET["id"]) or !$_GET["id"]) {
	http_response_code(400);
	die("The id argument was not provided.");
} else if (!isset($_GET["token"]) or !$_GET["token"]) {
	http_response_code(400);
	die("The token argument was not provided.");
}

require_once __DIR__ . "/../globals/reap_unclaimed_reservations.php";
reapUnclaimedReservations();

$dsn = "mysql:host=localhost;dbname=twinepm;";
$username = "tpm_emailvalidation_get_user";
$password = trim(file_get_contents("../get/tpm_emailvalidation_get_user.txt"));

$db = new PDO($dsn, $username, $password);

$stmt = $db->prepare("SELECT COUNT(id) FROM email_validation " .
	"WHERE id=? and token=?");

$stmt->execute(array((int)$_GET["id"], $_GET["token"]));
if ((int)$stmt->errorCode()) {
	http_response_code(500);
	die("An error was encountered while looking up the provided token.");
}

$fetch = $stmt->fetch(PDO::FETCH_NUM);
if (!(int)$fetch[0]) {
	http_response_code(404);
	die("No records can be found matching that name and token.");
}

$username = "tpm_passwords_post_user";
$password = trim(file_get_contents(__DIR__ . 
	"/../post/tpm_passwords_post_user.txt"));

$db = new PDO($dsn, $username, $password);

$stmt = $db->prepare("UPDATE passwords SET validated=1 WHERE id=?");
$stmt->execute(array((int)$_GET["id"]));

if ((int)$stmt->errorCode()) {
	http_response_code(500);
	die("There was an error setting the validation flag.");
}

die("Thank you for validating your e-mail. You may now log into TwinePM.");
?>
